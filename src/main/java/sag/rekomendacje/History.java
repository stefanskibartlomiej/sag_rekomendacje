package sag.rekomendacje;

import java.util.List;

/**
 * User: Bartlomiej Stefanski
 */
class History {
    private String key;
    private List<Recommendation> recommendations;

    History(String key, List<Recommendation> recommendations) {
        this.key = key;
        this.recommendations = recommendations;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public List<Recommendation> getRecommendations() {
        return recommendations;
    }

    public void setRecommendations(List<Recommendation> recommendations) {
        this.recommendations = recommendations;
    }
}
