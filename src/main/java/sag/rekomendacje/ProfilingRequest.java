package sag.rekomendacje;

import java.io.Serializable;

class ProfilingRequest implements Serializable {
    private String clientId;
    private String request;
    private String localisation;

    public ProfilingRequest(String clientId, String request, String localisation) {
        this.clientId = clientId;
        this.request = request;
        this.localisation = localisation;
    }

    public String getClientId() {
        return clientId;
    }

    public String getRequest() {
        return request;
    }

    public String getLocalisation() {
        return localisation;
    }
}
