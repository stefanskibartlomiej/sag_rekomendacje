package sag.rekomendacje;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

class DispatcherGui extends JFrame {
	private DispatcherAgent agent;
	
	private JTextField idField;
    private JTextField requestField;
    private JTextField localisationField;

	DispatcherGui(DispatcherAgent agent) {
        super(agent.getLocalName());

        this.agent = agent;

		JPanel p = new JPanel();
		p.setLayout(new GridLayout(3, 2));
		p.add(new JLabel("Identyfikator uzytkownika:"));
		idField = new JTextField(15);
		p.add(idField);
		p.add(new JLabel("Request:"));
		requestField = new JTextField(15);
		p.add(requestField);

        p.add(new JLabel("Lokalizacja:"));
        localisationField = new JTextField(15);
        p.add(localisationField);
		getContentPane().add(p, BorderLayout.CENTER);
		
		JButton sendButton = new JButton("Wyslij");
		sendButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent ev) {
                try {
                    String id = idField.getText().trim();
                    String request = requestField.getText().trim();
                    String localisation = localisationField.getText().trim();
                    DispatcherGui.this.agent.handleClientRequest(id, request, localisation);
                    idField.setText("");
                    requestField.setText("");
                    localisationField.setText("");
                } catch (Exception e) {
                    JOptionPane.showMessageDialog(DispatcherGui.this, "Nieprawidlowe wartosci. " + e.getMessage(), "Blad", JOptionPane.ERROR_MESSAGE);
                }
            }
        });
		p = new JPanel();
		p.add(sendButton);
		getContentPane().add(p, BorderLayout.SOUTH);
		
		addWindowListener(new	WindowAdapter() {
			public void windowClosing(WindowEvent e) {
				DispatcherGui.this.agent.doDelete();
			}
		} );
		
		setResizable(false);
	}
	
	public void display() {
		pack();
		Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
		int centerX = (int)screenSize.getWidth() / 2;
		int centerY = (int)screenSize.getHeight() / 2;
        // (BS) wyłączyłem centrowanie okna - nakładało się z GUI Jade'a
		//setLocation(centerX - getWidth() / 2, centerY - getHeight() / 2);
		setVisible(true);
	}	
}
